---
title: DocOps registry
leads:
  - "Bryan Klein"
meeting: Bi-weekly on Thursdays (US)/Fridays (APAC) - Check time on [Community calendar](https://thegooddocsproject.dev/community/#calendar)
status: Active
description: An open registry of technologies, tooling, and methodologies used in creating, managing, and releasing documentation. Bonus points are given to stacks that are compatible with The Good Docs Project templates.
slackName: doc-ops-registry
slackID: C01T803GT34
notesURL: https://docs.google.com/document/d/1zOn8WX1if8Bzk2yKmQtwnPU3QGqOzdDrtaa5x03Y2M0/edit?usp=sharing
draft: false
---

This group is researching the various tools that could be used in a documentation system to set up a new doc site or improve any existing one. The doc tools registry is building a database that allows you to filter the available tools based on your system requirements.
