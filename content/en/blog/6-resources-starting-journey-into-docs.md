---
draft: false
title: 6 resources for starting your journey into docs!
date: 2022-07-16T21:58:42.336Z
publishDate: 2022-07-19T21:58:42.336Z
lastmod: 2022-09-26T17:06:12.081Z
image: /uploads/blog/headers/Matthew-Sleeper.jpg
images:
  - /uploads/blog/headers/Matthew-Sleeper.jpg
author:
  - tina_luedtke
tags:
  - writing-tips
  - learn-tech-writing
custom_copyright: ""
custom_license: Photo by [Matthew
  Sleeper](https://unsplash.com/@mjsleeper?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
  on
  [Unsplash](https://unsplash.com/s/photos/begin-idea?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText).
type: blog
---
Do you find yourself in the shoes of someone who has to write documentation for the first time ever? The closer you look, the more you will find that technical writing is actually quite a broad discipline. Amidst the tons of resources, it can be hard to figure out what you need to know to get started. So here are six resources to narrow that space for you.

## Learning the basics

It can be tempting to start writing down your thoughts all in one document. But what makes perfect sense to you isn't necessarily easy to digest for your users. In technical writing, scoping information and ordering it logically are critical to keep your audience from getting lost. 

✍️ [Google Tech Writing Course](https://developers.google.com/tech-writing): Tech writers at Google developed this course for software engineers they work with. It covers the very basics of writing understandable documentation. It is a great starting point, especially if you feel like you're not a strong writer (yet).

📚 [Docs for Developers](https://docsfordevelopers.com/): This book explains beautifully what to focus on when you build your documentation for the first time. In contrast to the Google tech writing course, the book focuses less on the stylistic elements of writing and more on the whole documentation process from conception to publication and measuring documentation quality. I definitely recommend reading it before starting your writing process. 

## Writing - Let's get to it

After working on your foundational understanding of technical writing, you will have a clearer picture about how to organize your content and how to scope it. Next comes the hard part: getting words on to a blank page. 

📄 [The Good Docs project templates](https://gitlab.com/tgdp/templates/-/blob/main/README.md#the-templates): This repository contains all the guts and glory that the Good Docs project is working on: **Templates**! Our aim is to help you write awesome documentation. If you have questions about how to use a template or want to work on a template, join our [Good Docs Slack channel](https://join.slack.com/t/thegooddocs/shared_invite/zt-be2gay0m-Ukq_5SI0MHp20IQP3auQjg). We look forward to seeing you around! 

📺 [Write the Docs YouTube channel](https://www.youtube.com/c/WritetheDocs/videos): Sometimes we face writing struggles and don't know how to solve them. Most likely, someone else had to overcome a similar challenge and has shared their knowledge in the form of a talk. You can find practical topics like selecting a documentation tool, crafting good error messages, and many other well-crafted talks on the Write the Docs YouTube channel.

## Networking - It takes a village 

When I am learning something new, my mind is buzzing, and I want to bounce around ideas. Luckily, you can find like-minded people from all over the world in wonderful documentation communities. 

🌐 [Write the Docs](https://www.writethedocs.org/): This group consists of documentarians who mostly work on software. Write the Docs holds networking events like Meetups and conferences, and hosts a Slack channel for in-between communication. The community is very diverse in terms of location, age, and identities. It's a warm and welcoming place with a lot of friendly faces to bounce around ideas with.

🌐 [Society for Technical Communication](https://www.stc.org/): The Society for Technical Communication (STC) is an American professional organization for technical writers from every field and domain you can imagine - from government-related domains, to hardware documentation, to software documentation. STC holds conferences and chapter meetings, which are free most of the time. STC also fosters relations with the academical side of documentation and provides certification and regular publications; these require a paid membership. 

## Tip of the iceberg

The technical writing discipline is dynamic and growing, and more knowledge is being shared every day.  What helpful resources have you discovered during your tech writing journey? Share them in our [Good Docs Project Slack](https://join.slack.com/t/thegooddocs/shared_invite/zt-18gk5udo6-3OG5VVbFEvv~uG8y8tCDlQ) or [@ us on Twitter](https://twitter.com/thegooddocs)!

Happy writing!
