---
title: Who We Are
lastmod: 2023-05-18T03:44:54.217Z
---

{{% blocks/lead color="primary" %}}

# Who We Are

This page lists the people who currently contribute in key roles in the Good Docs Project and what their responsibilities are.

{{% /blocks/lead %}}

{{% blocks/section color="white" type="section" %}}

The Good Docs Project is powered by a great community and a team of dedicated folks who keep our project moving forward.
We are always looking for more people to help with:

- Running and participating in working groups.
- Onboarding and welcoming new community members.
- Assisting with the project's IT needs.


If you're interested in helping out, get in touch with someone from the team you're interested in.
The best way to contact these team members is on our Slack workspace, which you can get invited to by attending one of our [Welcome Wagon](/welcome) meetings.

## List of teams

- [Project steering committee](#project-steering-committee)
- [Community managers](#community-managers)
- [Tech team](#tech-team)
- [Template product management team](#template-product-management-team)
- [Working group leads](#working-group-leads)
- [Thank you to past team members](#thank-you-to-past-team-members)


## Project steering committee

The Project Steering Committee (PSC) is the official managing body for our project.

This team:

- Has a duty to define and uphold the project's mission, goals, and plans.
- Nominates and votes to approve individuals to key community roles and teams.
- Discusses and votes on project business and major project initiatives, such as requests-for-comment (RFCs).
- Is chaired by 1-3 individuals, who act as tie-breaking votes and who help ensure the efficiency and effectiveness of the PSC.


The current project steering committee members are:

- Aaron Peters, co-chair
- Carrie Crowe, co-chair
- Tina Lüdtke, co-chair
- Alyssa Rock
- Bryan Klein
- Cameron Shorter
- Deanna Thompson
- Erin McKean
- Felicity Brand
- Gayathri Krishnaswamy
- Mengmeng Tang
- Michael Park
- Valeria Hernandez

The committee is drawn from members of the community and is based on merit and interest.
The project steering committee:

- Aims to attract a diverse mix of personal characteristics, reflective of the community we represent.
- Avoids having employees of any one organization representing a controlling proportion of the PSC.
- May retire any time. If a PSC member becomes inactive for multiple quarters, it may be time to retire. (Ex-members will typically be welcomed back if they become active again.) An inactive member may be invited to retire. If unresponsive, they may be removed by the existing PSC.

If you are interested in serving on the PSC, let a current member of the PSC know about your interest.


## Community managers

This community managers ensure our community is healthy, vibrant, and safe for all who want to participate.

This team is responsible for:

- Fostering a positive community culture.
- Growing our community.
- Onboarding, mentoring, and training community members.
- Ensuring our community is safe and supported by:
  - Handling [Code of Conduct](/code-of-conduct) incidents.
  - Moderating our community forums.

The current community managers are:

- Alyssa Rock
- Carrie Crowe
- Deanna Thompson

If you are interested in serving as a community manager, let one of the community managers know.
You can also join the bi-weekly community onboarding and training sync to start volunteering to assist the community managers.

If you experience an incident which makes you feel less safe in our community or less able to be your authentic self, contact the community manager with whom you have the most comfortable relationship and let them know.

The best way to contact these team members is on our Slack workspace in the **#ask-a-community-manager** channel or through a private direct message.
You can get invited to our Slack workspace by attending one of our [Welcome Wagon](/welcome) meetings.

For more information, see:

- [The Good Docs Project Code of Conduct](/code-of-conduct)


## Tech team

The Tech team helps with the project’s IT services and tooling needs, including repository access.

This team:

- Maintains access to all the project's IT services and tools.
- Enforces and maintains our security policies.
- Consults on IT related requests and needs.

The current Tech team members are:

- Alyssa Rock
- Bolaji Ayodeji
- Bryan Klein
- Michael Park

If you are interested in joining the Tech team, let one of the members of this team know.
You can also join the bi-weekly DocOps Registry working group, which acts as an entry point for this team.

The best way to contact the team is on our Slack workspace in the **#tech-requests** channel.
You can get invited to our Slack workspace by attending one of our [Welcome Wagon](/welcome) meetings.


## Template product management team

The template product management team are responsible for defining the vision of the template suite product and related value streams.

This team:

- Creates the template product roadmap.
- Conducts user research and gathers feedback to determine what initiatives should go into the roadmap.
- Coordinates with the template working group to put the roadmap into action (by creating issues to track work, defining priorities, and more).
- Ensures our templates have a consistent style and voice.

The current template product team members are:

- Tina Lüdtke (lead)
- Alyssa Rock (support)
- Ane Troger (style guide lead)

If you are interested in joining the template product management team, let one of the members of this team know.

The best way to contact the team is on our Slack workspace in the **#project-roadmap** channel.
You can get invited to our Slack workspace by attending one of our [Welcome Wagon](/welcome) meetings.


## Working group leads

The working group leads help manage our project's various working groups.

The current working group leads for each working group are:

<table>
  <tbody>
    <tr>
      <th>Working group</th>
      <th>Lead(s)</th>
    </tr>
    <tr>
      <td>Team Alpaca Templates and Chronologue (US/APAC)</td>
      <td>
        <ul>
          <li>Alyssa Rock</li>
          <li>Deanna Thompson</li>
          <li>Michael Park</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>Team Macaw Templates (APAC/EMEA)</td>
      <td>
        <ul>
          <li>Cameron Shorter</li>
          <li>Gayathri Krishnaswamy</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>Team Dolphin Templates and Chronologue (US/EMEA)</td>
      <td>
        <ul>
          <li>Alyssa Rock</li>
          <li>Michael Hungbo</li>
          <li>Valeria Hernandez</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>DocOps registry</td>
      <td>
        <ul>
          <li>Bryan Klein</li>
          <li>Michael Park</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>UX and outreach</td>
      <td>
        <ul>
          <li>Alyssa Rock</li>
          <li>Carrie Crowe</li>
          <li>Tina Lüdtke</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

To join any of these working groups, see the [Community Calendar](/community) for meeting times.



## Thank you to past team members

We are thankful for the hard work and dedication of these past team members:

Previous PSC members:

- Aidan Doherty
- Ankita Tripathi
- Becky Todd
- Clarence Cromwell
- Jared Morgan
- Jennifer Rondeau
- Jo Cook
- Morgan Craft
- Nelson Guya
- Ryan Macklin
- Viraji Ogodapola


{{% /blocks/section %}}
