---
title: Templates Feedback Form
linkTitle: Welcome
lastmod: 2023-07-26T01:03:16.008Z
---

{{% blocks/lead color="primary" %}}

# Feedback form for The Good Docs Project templates

Tell us how we can improve!

{{% /blocks/lead %}}

{{% blocks/section color="white" type="section" %}}

{{< template-feedback >}}

{{% /blocks/section %}}
